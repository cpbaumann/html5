<?php


if (!extension_loaded('curl')) {
        echo 'curl extension not loaded.';
}


    #http://php.net/manual/de/function.curl-getinfo.php

    Class GetPage{

        public static function request($url){


            $time_start = date('d.m.Y H:i:s');
            $info = [];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLINFO_HEADER_OUT , 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:57.0) Gecko/20100101 Firefox/57.0');
            $content = curl_exec($ch);
            $info = curl_getinfo($ch);

            if(!curl_errno($ch)){
                $info['content_urlencode'] = urlencode($content);
            }

            if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                #echo "cURL error ({$errno}):\n {$error_message}";
                $info['curl_error'] = $error_message;
            }

            $info['time_start'] = $time_start;
            curl_close($ch);
            return $info;
        }   
    }