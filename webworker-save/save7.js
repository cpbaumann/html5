/*
self.addEventListener("message", function (e) {

	var data = JSON.stringify(e.data);
	var req = new XMLHttpRequest();
	// https://wiki.selfhtml.org/wiki/JavaScript/XMLHttpRequest/open
	req.open("POST", "http://localhost/html5/webworker-save/save.php", false);
	req.setRequestHeader("Content-Type", "application/json");
	req.onreadystatechange = function () {
		if (req.readyState == 4 && req.status == 200) {
			self.postMessage({ "Error": "No", "Message": "Save very Successful 0" });
		}
	}
	req.send(data);
});

*/




self.addEventListener('message', function(e) {
	var data = JSON.stringify(e.data);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            self.postMessage({"Message": 'Message: ' + xmlhttp.responseText});
        }
    }
    xmlhttp.open('POST','save.php',true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(data);
}, false);

