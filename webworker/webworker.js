function calcFib() {
  var startTime = new Date(),
      i = 2,
      fib = [],
      fib_output = "";

  fib[0] = 0;
  fib[1] = 1;

  while(1)
  {
      fib[i] = fib[i-2] + fib[i-1];
      if (fib[i] === Number.POSITIVE_INFINITY)
        break;
      fib_output += fib[i] + "<br>";
      i++;
  }

  var totalTime = (new Date() - startTime);

  console.dir(totalTime)
    console.dir(fib_output)

  return [
    fib_output,
    (i+2) + " Fibonacci Numbers generated in " + totalTime + " ms."
  ];
}

self.onmessage = function(e) {
    self.postMessage(calcFib());
};