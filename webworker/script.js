$(function() {
  if (window.Worker) {
    var WEB_WORKER_URL = "http://localhost/html5/webworker/webworker.js";
    var worker = new Worker(WEB_WORKER_URL);

    worker.onmessage = function(e) {
      $("#Fib_Output").append(e.data[0]);
      $("#Info_Output").append(e.data[1]);
    }

    // Start the worker
    worker.postMessage("");
  }
  else {
    $("#Fib_Output").html("<h1>Your browser does not support Web Workers.<br/>Please use a modern browser.</h1>");
  }
});