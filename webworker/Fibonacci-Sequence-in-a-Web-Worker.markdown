Fibonacci Sequence in a Web Worker
----------------------------------
Generates the Fibonacci Sequence in a Web Worker.

A [Pen](http://codepen.io/AndrewBarfield/pen/ZYeXPV) by [Andrew Mitchell Barfield](http://codepen.io/AndrewBarfield) on [CodePen](http://codepen.io/).

[License](http://codepen.io/AndrewBarfield/pen/ZYeXPV/license).