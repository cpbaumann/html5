(function ($) {
    "use strict";
    $.fn.extend({

        modal: function (options) {
            var defaults = { close: null },
                overlay = $("<div id='modal-overlay'></div>");

            $("body").append(overlay);
            options =  $.extend(defaults, options);
            return this.each(function () {
                var option = options;
                $(this).click(function (e) {
                    e.preventDefault();
                    $(overlay).show();
                    var id = $(this).attr("href");
                    $(id).css({
                        'position': 'fixed', 'z-index': 1,
                        'left': 50 + '%', 'margin-left': -($(id).outerWidth() / 2) + 'px',
                        'top': 50 + '%', 'margin-top': -($(id).outerHeight() / 2) + 'px'
                    }).show();
                    $(overlay).add(option.close).click(function (e) {
                        e.preventDefault();
                        $(overlay).add(id).hide();
                    });
                });
            });
        }
    });
})(jQuery);