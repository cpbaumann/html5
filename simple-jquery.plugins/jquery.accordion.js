(function ($) {
    "use strict";
    $.fn.extend({
        accordion: function (options) {

            var defaults = {
                heading: "h2",
                openfirstitem: false
            },
            option = $.extend(defaults, options);

            return this.each(function () {
                var obj = $(this),
                    heading = option.heading,
                    openfirstitem = option.openfirstitem,
                    head = $(heading, obj);

                head.next().hide();

                if (true === openfirstitem) {
                    head.next().eq(0).show();
                    head.eq(0).addClass('active');
                }

                head.click(function () {
                    if (!$(this).hasClass('active')) {
                        head.removeClass('active').next().slideUp();
                        $(this).addClass('active').next().slideDown();
                    } else {
                        $(this).removeClass('active').next().slideUp();
                    }
                });
            });
        }
    });
})(jQuery);
