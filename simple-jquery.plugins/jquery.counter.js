(function( $ ) {
  "use strict";
  $.fn.extend({
    counter: function (options) {
      var settings = {
        limit      : undefined,
        direction  : 'up',
        onhitLimit : function() {},
        fadeOut    : 500,
        fadeIn     : 100
      };

      settings = $.extend(settings, options);

      var obj = this,
          count = settings.direction == 'up' ? 1 : (settings.limit);

      (function toggleFader() {

        var inlimit = false;
        obj.fadeTo( settings.fadeOut, 1, function() {

          if (settings.direction == 'up'){
            count++;
            inlimit = (settings.limit && (count > settings.limit));
          }

          if (settings.direction == 'down'){
            count--;
            inlimit = (count <= 0);
          }

          obj.fadeTo(settings.fadeIn, 0, function() {
            
            if (true === inlimit) {
              clearTimeout(rotation);
              settings.onhitLimit.call();
            }
          });

        }).html(count);
        
        if (false === inlimit) {
          var rotation = setTimeout(toggleFader, 1000); 
        }

      })();

      return this; 
    }
  });
})(jQuery);