/* 
* Author: cpbaumann
*/

;(function($, window, undefined) {
    'use strict';        

    $.modalresponsive = function(options, element) {
        this.obj = $(element);
        this._init(options); 
    };

    $.modalresponsive.defaults = {
        close: null,
        overlayerHTML : '<div class="modal-overlay"></div>', 
        overlayerIdPrefix: 'modal-overlay-'
    };

    $.modalresponsive.prototype = {
      
        /**
        * Private methods (set private by '_')
        */
        _init: function(options) {
            this.options = $.extend(true, {}, $.modalresponsive.defaults, options);

            this.objId  = this.obj.attr('id');
            this.modalcontentId = this.obj.attr('href');
            this.overlayer = $(this.options.overlayerHTML);
            this.overlayer.attr('id', this.options.overlayerIdPrefix + this.objId);
            this._useraction();

        }, 

        _useraction: function() {
            
            var overlayer = this.overlayer,
                modalcontentId = this.modalcontentId,
                element = this.obj,
                options = this.options;

            element.bind('click touch', function(e) {
                e.preventDefault();

                $('body').append(overlayer);
                $(overlayer).show();

                $(modalcontentId).css({
                    'position': 'fixed', 
                    'z-index': 99,
                    'left': '50%', 
                    'margin-left': -($(modalcontentId).outerWidth() / 2) + 'px',
                    'top': '50%', 
                    'margin-top': -($(modalcontentId).outerHeight() / 2) + 'px'
                    }).show();
            });
            
            overlayer.bind('click touch', function (e) {
                e.preventDefault();
                $(modalcontentId).removeAttr('style'); 
                $(overlayer).removeAttr('style'); 
            });

            $(options.close).bind('click touch', function (e) {
                e.preventDefault();
                $(modalcontentId).removeAttr('style'); 
                $(overlayer).removeAttr('style'); 
            });
        },

        /**
        * Public methods
        */
        destroy: function() {

            $('#' + this.options.overlayerIdPrefix + this.objId).remove(); 
            $(this.modalcontentId).removeAttr('style'); 
            this.obj.unbind();
            this.obj.removeData();
            console.log('-- clear one plugin instance --');
        }
    }; 


    $.fn.modalresponsive = function(options) { 
        
        this.each(function () {
            var self = $.data(this, 'modalresponsive');
            if( typeof options === 'string' ) {
                var args = Array.prototype.slice.call(arguments, 1);
                if ( !self ) {
                    console.log(' -- cannot call methods prior to initialization of modalresponsive -- '); 
                    console.log(' -- attempted to call method ' + options + ' -- ');  
                    return;               
                }

                if( !$.isFunction(self[options]) || options.charAt(0) === '_' ) {
                    console.log(' -- no such public method in plugin modalresponsive' + options + ' -- ');
                    return;                                                     
                }

                self[options].apply(self, args);

            } else {     
                if(self) {
                    self._init(); 
                    console.log(' -- plugin modalresponsive is allready runnig -- '); 
                } else {             
                    self = $.data(this, 'modalresponsive', new $.modalresponsive(options, this));
                    console.log(' -- plugin modalresponsive initialization -- ');
                }
            } 
        });

        return this;
    }; 
})(jQuery, window);