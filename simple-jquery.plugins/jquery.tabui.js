(function ($) {
    "use strict";
    $.fn.extend({
        tabui: function () {

            return this.each(function () {

                var obj = $(this),
                    blocks = $('div', obj),
                    tabs = $('li', obj),
                    links = $('li a', obj);

                blocks.hide();
                blocks.eq(0).show();
                tabs.eq(0).addClass("active");

                links.click(function (e) {
                    e.preventDefault();
                    var url = $(this).attr('href'),
                        hash = url.substring(url.indexOf("#"));

                    tabs.removeClass("active");
                    blocks.hide();
                    $(this).parent().addClass("active");
                    $(hash).show();

                });
            });
        }
    });
})(jQuery);