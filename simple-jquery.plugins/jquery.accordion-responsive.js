/* 
* Author: cpbaumann
*/

;(function($, window, undefined) {
    'use strict';        

    $.accordionresponsive = function(options, element) {
        this.obj = $(element);
        this._init(options); 
    };

    $.accordionresponsive.defaults = {
        heading: "h2",
        activestateClass: 'active',
        openItem: false,        
        showallButtom: false
    };

    $.accordionresponsive.prototype = {
      
        /**
        * Private methods (set private by '_')
        */
        _init: function(options) {
            // options
            this.options = $.extend(true, {}, $.accordionresponsive.defaults, options);
            this.head = $(this.options.heading, this.obj);
            this._intitialstate();
            this._useraction_showItem(this.head,this.options);
            if (this.options.showallButtom !== false) {
                this._useraction_showallItems(this.head,this.options);
            }
        }, 

        _intitialstate: function() {

            this.head.next().hide();
            // set open item or not on default value false
            if (parseInt(this.options.openitem)) {
                this.head.next().eq(this.options.openitem - 1).show();
                this.head.eq(this.options.openitem -1 ).addClass(this.options.activestateClass);
            }
        },                                                               

        _useraction_showItem: function(element,options) {

            element.bind('click touch', function() {
                if (!$(this).hasClass(options.activestateClass)) {
                    element.removeClass(options.activestateClass).next().slideUp();
                    $(this).addClass(options.activestateClass).next().slideDown();
                } else {
                    $(this).removeClass(options.activestateClass).next().slideUp();
                }
            });
        },

        _useraction_showallItems: function(element,options) {

            $(options.showallButtom).bind('click touch', function() {
                if (!$(this).hasClass(options.activestateClass)) {
                    element.addClass(options.activestateClass).next().slideDown();
                    $(this).addClass(options.activestateClass).html( $(this).data('text-hide') );
                } else {
                    element.removeClass(options.activestateClass).next().slideUp();
                    $(this).removeClass(options.activestateClass).html( $(this).data('text-show') )
                }
            });
        },
        /**
        * Public methods
        */
        destroy: function() {

            // remove hide()/show() style attr
            this.head.next().removeAttr('style'); 
            this.head.removeClass(this.options.activestateClass);
            this.head.unbind();
            if (this.options.showallButtom !== false ) {
                $(this.options.showallButtom)
                    .html( $(this.options.showallButtom).data('text-show') )
                    .removeClass(this.options.activestateClass);
                $(this.options.showallButtom).unbind();      
            }

            this.obj.removeData();
            console.log('-- clear one plugin instance --');
        }
    }; 


    $.fn.accordionresponsive = function(options) { 
        
        this.each(function () {

            var self = $.data(this, 'accordionresponsive');
            if( typeof options === 'string' ) {
                var args = Array.prototype.slice.call(arguments, 1);
                if ( !self ) {
                    console.log(' -- cannot call methods prior to initialization of accordionresponsive -- '); 
                    console.log(' -- attempted to call method ' + options + ' -- ');  
                    return;               
                }

                if( !$.isFunction(self[options]) || options.charAt(0) === '_' ) {
                    console.log(' -- no such public method in plugin accordionresponsive' + options + ' -- ');
                    return;                                                     
                }

                self[options].apply(self, args);

            } else {     
                if(self) {
                    self._init(); 
                    console.log(' -- plugin accordionresponsive is allready runnig -- '); 
                } else {             
                    self = $.data(this, 'accordionresponsive', new $.accordionresponsive(options, this));
                    console.log(' -- plugin accordionresponsive initialization -- ');
                }
            } 
        });

        return this;
    }; 
})(jQuery, window);