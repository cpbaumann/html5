
<!doctype html>
<html lang="de"> 
<head>
	<meta charset="utf-8">




</head>
<body>



  ()[]\;:,<>@example.com (keines der Zeichen vor dem @ ist ausserhalb von Hochkommas ("") erlaubt) <br>


  <?php echo str_rot13("<a href='mailto: 1someone[@]somedomani.com'>1someone[@]somedomani.com</a>");?> becomes
 <span class='encode'><?php echo str_rot13("<a href='mailto: 1someone[@]somedomani.com'>1someone[@]somedomani.com</a>");?></span>





<br>


https://codereview.stackexchange.com/questions/132125/rot13-javascript


<script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>

<script>


(function($) {
    $('.encode').each( function(count,enc) { //foreach encoded DOM element
        encodedData = jQuery(enc).html(); //grab encoded text

        encodedData = encodedData.replace(/\[|\]/g, "");

        decodedData = encodedData.replace(/[a-zA-Z]/g, function(char){ //foreach character
            return String.fromCharCode( //decode string
                /**
                 * char is equal/less than 'Z' (i.e. a  capital letter), then compare upper case Z unicode 
                 * else compare lower case Z unicode.
                 * 
                 * if 'Z' unicode greater/equal than current char (i.e. char is 'Z','z' or a symbol) then
                 * return it, else transpose unicode by 26 to return decoded letter
                 *
                 * can't remember where I found this, and yes it makes my head hurt a little!
                 */
                (char<="Z"?90:122)>=(char=char.charCodeAt(0)+13)?char:char -26
            );
        });
        jQuery(enc).html(decodedData); // replace text
    });
})(jQuery); 
</script>
</body>
</html>
