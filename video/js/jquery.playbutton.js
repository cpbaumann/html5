/*!
 * playbutton plugin 1.0 
 * by C.P. Baumann - http://baumann-onlinemedien.de
 *
 */
;(function($, window, undefined) {
    'use strict';        

    $.playbutton = function(options, element) {
        this.obj = $(element);
        this._init(options); 
    };

    $.playbutton.defaults = {
        playbuttonClass: '.playpause',
        videoClass: '.video',
        videoControls: '.video-controls'
    };

    $.playbutton.prototype = {
      
        _init: function(options) {

            this.options = $.extend(true, {}, $.playbutton.defaults, options);

            this.button = $(this.options.playbuttonClass, this.obj);
            this.controls = $(this.options.videoControls, this.obj);
            this.videoelement = $(this.options.videoClass, this.obj)[0]; 

            this._intitialstate();
            this._useraction(this.button,this.options, this.videoelement);
        }, 

        _intitialstate: function() {
           this.videoelement.controls = false;
           this.controls.css('display','block');
        },                                                               

        _useraction: function(element,options,target) {

            element.bind('click touch', function() {
                if ((target.paused || target.ended)) {
                    $(this).attr("data-state","play");
                    target.play(); 
                } else {
                    $(this).attr("data-state","pause");
                    target.pause(); 
                }
            });   
        },

        destroy: function() {

            this.button.attr("data-state","");
            this.button.unbind();
            this.videoelement.controls = true;
            this.controls.css('display','none');
            this.obj.removeData();
            console.log('-- clear one plugin instance of playbutton --');
        }
    }; 

    $.fn.playbutton = function(options) { 
        
        this.each(function () {

            var self = $.data(this, 'playbutton');
            if( typeof options === 'string' ) {
                var args = Array.prototype.slice.call(arguments, 1);
                if ( !self ) {
                    console.log(' -- cannot call methods prior to initialization of playbutton -- '); 
                    console.log(' -- attempted to call method ' + options + ' -- ');  
                    return;               
                }

                if( !$.isFunction(self[options]) || options.charAt(0) === '_' ) {
                    console.log(' -- no such public method in plugin playbutton' + options + ' -- ');
                    return;                                                     
                }

                self[options].apply(self, args);

            } else {     
                if(self) {
                    self._init(); 
                    console.log(' -- plugin playbutton is allready runnig -- '); 
                } else {             
                    self = $.data(this, 'playbutton', new $.playbutton(options, this));
                    console.log(' -- plugin playbutton initialization -- ');
                }
            } 
        });

        return this;
    }; 
})(jQuery, window);