<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title>play button</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/video.css" rel="stylesheet">
</head>
<body id="top"> 
  <div class="container">
    <header>
      https://developer.mozilla.org/en-US/Apps/Fundamentals/Audio_and_video_delivery/cross_browser_video_player#PlayPause
      <br>
      https://developer.mozilla.org/en-US/Apps/Fundamentals/Audio_and_video_delivery/Video_player_styling_basics

    </header> 
    <main>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <figure id="videoContainer-1">
          <div class="video-wrapper">
           <video class="video" controls preload="metadata" poster="img/poster.jpg">
            <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
              <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
              </video>
              <div class="video-controls">
               <button class="playpause" data-state="pause">Play/Pause</button>
             </div>
           </div>
             <figcaption>1.<br><br>br></figcaption>
           </figure>
         </div>
         <div class="col-xs-12 col-sm-6">

          <figure id="videoContainer-2">
          <div class="video-wrapper">
           <video class="video" controls preload="metadata" poster="img/poster.jpg">
            <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
              <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
              </video>
              <div class="video-controls">
               <button class="playpause" data-state="pause">Play/Pause</button>
             </div>
           </div>
             <figcaption>2.</figcaption>
           </figure>
         </div>
       </div>
       <div class="row">
         <div class="col-xs-12 col-sm-6">

          <figure id="videoContainer-3">
            <div class="video-wrapper">
           <video class="video" controls preload="metadata" poster="img/poster.jpg">
            <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
              <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
              </video>
              <div class="video-controls">
               <button class="playpause" data-state="pause">Play/Pause</button>
             </div>
           </div>
             <figcaption>3.<br>
              <code><pre>
                $('#videoContainer-3').playbutton();
                $('#videoContainer-3').playbutton('destroy');
              </pre>
            </code></figcaption>
          </figure>
        </div>
        <div class="col-xs-12 col-sm-6">

          <figure id="videoContainer-4">
           <video class="video" controls preload="metadata" poster="img/poster.jpg">
            <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
              <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
              </video>
              <figcaption>4.<br>
                default GUI HTML5 Video
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <figure class="videoContainer">
              <div class="video-wrapper">
             <video class="video" controls preload="metadata" poster="img/poster.jpg">
              <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
                <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
                </video>
                <div class="video-controls">
                 <button class="playpause" data-state="pause">Play/Pause</button>
               </div>
             </div>
               <figcaption>5.<br>
                <code><pre>
                  $('.videoContainer').playbutton();
                </pre>
              </code></figcaption>
            </figure>
          </div>
          <div class="col-xs-12 col-sm-6">
            <figure class="videoContainer">
              <div class="video-wrapper">
             <video class="video" controls preload="metadata" poster="img/poster.jpg">
              <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
                <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
                </video>
                <div class="video-controls">
                 <button class="playpause" data-state="pause">Play/Pause</button>
               </div>
             </div>
               <figcaption>6.<br>
                <code><pre>
                  $('.videoContainer').playbutton();
                </pre>
              </code></figcaption>
            </figure>
          </div>
        </div>



      </main>
      <footer>
      </footer>
    </div> <!-- /container -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.playbutton.js"></script>
    <script>

    $('#videoContainer-1').playbutton();
  //$('#videoContainer-1').playbutton('destroy');
  $('#videoContainer-2').playbutton();
  //$('#videoContainer-2').playbutton('destroy');

  $('#videoContainer-3').playbutton();
  $('#videoContainer-3').playbutton('destroy');

   //  $('#videoContainer-4').playbutton();
  //$('#videoContainer-4').playbutton('destroy');

  $('.videoContainer').playbutton();
  //$('.videoContainer').playbutton('destroy');

   /* 
var video = $('#videoContainer .video')[0];
  // Hide the default controls
  video.controls = false;

  // Display the user defined video controls
  $('#videoContainer .video-controls').css('display','block');
  $('#videoContainer .playpause').on('click', function() {
    console.log('hit');
    if ((video.paused || video.ended)) {
      video.play(); 
      $('#videoContainer .playpause').attr("data-state","play");
    } else {
      video.pause(); 
      $('#videoContainer .playpause').attr("data-state","pause");
    }
  });
*/
</script>
</body>
</html>