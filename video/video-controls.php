<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title></title>
  <link href="/css/bootstrap.css" rel="stylesheet">
  <style>
  figure{
    position: relative;
  }
  video::-webkit-media-controls {
    display: none !important;
  }
  </style>
</head>
<body id="top"> 
  <div class="container">
    <header>
      https://developer.mozilla.org/en-US/Apps/Fundamentals/Audio_and_video_delivery/cross_browser_video_player
    </header> 
    <main>
     <figure id="videoContainer">

      <video id="video" controls preload="metadata" poster="/img/poster.jpg">
        <source src="video/Der-PM-14-18-im-VdS-Test.webm" type="video/webm">
          <source src="video/Der-PM-14-18-im-VdS-Test.mp4" type="video/mp4">
          </video>
          <figcaption>figcaption</figcaption>
        </figure>
        <ul id="video-controls" class="controls">
          <li><button id="playpause" type="button">Play/Pause</button></li>
          <li><button id="stop" type="button">Stop</button></li>
          <li class="progress">
            <progress id="progress" value="0" min="0">
              <span id="progress-bar"></span>
            </progress>
          </li>
          <li><button id="mute" type="button">Mute/Unmute</button></li>
          <li><button id="volinc" type="button">Vol+</button></li>
          <li><button id="voldec" type="button">Vol-</button></li>
          <li><button id="fs" type="button">Fullscreen</button></li>
        </ul>
      </main>
      <footer>
      </footer>
    </div> <!-- /container -->
    <script src="js/jquery.js"></script>
    <script>
  //https://developer.mozilla.org/en-US/Apps/Fundamentals/Audio_and_video_delivery/cross_browser_video_player#PlayPause
  var video = document.getElementById('video');
  var videoControls = document.getElementById('video-controls');
  // Hide the default controls
  video.controls = false;

  // Display the user defined video controls
  videoControls.style.display = 'block';
  var playpause = document.getElementById('playpause');
  var stop = document.getElementById('stop');
  var mute = document.getElementById('mute');
  var volinc = document.getElementById('volinc');
  var voldec = document.getElementById('voldec');
  var progress = document.getElementById('progress');
  var progressBar = document.getElementById('progress-bar');
  var fullscreen = document.getElementById('fs');

  playpause.addEventListener('click', function(e) {
   if (video.paused || video.ended) video.play();
   else video.pause();
 });

  stop.addEventListener('click', function(e) {
   video.pause();
   video.currentTime = 0;
   progress.value = 0;
 });

  mute.addEventListener('click', function(e) {
   video.muted = !video.muted;
 });

  volinc.addEventListener('click', function(e) {
   alterVolume('+');
 });

  voldec.addEventListener('click', function(e) {
   alterVolume('-');
 });

  var alterVolume = function(dir) {
    var currentVolume = Math.floor(video.volume * 10) / 10;

    if (dir === '+') {
      if (currentVolume < 1) {
        video.volume += 0.1;
      }

    }else{
      if (dir === '-') {
        if (currentVolume > 0) {
          video.volume -= 0.1;
        }
      }
    }

    video.addEventListener('loadedmetadata', function() {
     progress.setAttribute('max', video.duration);
   });

    video.addEventListener('timeupdate', function() {
      if (!progress.getAttribute('max')) progress.setAttribute('max', video.duration);
      progress.value = video.currentTime;
      progressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%';
    });

    var fullScreenEnabled = !!(document.fullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled || document.webkitSupportsFullscreen || document.webkitFullscreenEnabled || document.createElement('video').webkitRequestFullScreen);
    if (!fullScreenEnabled) {
      fullscreen.style.display = 'none';
    }

    var isFullScreen = function() {
      return !!(document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
    }

    fs.addEventListener('click', function(e) {
      handleFullscreen();
    });

    var handleFullscreen = function() {
      if ( isFullScreen() ) {
        if (document.exitFullscreen) document.exitFullscreen();
        else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
        else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
        else if (document.msExitFullscreen) document.msExitFullscreen();
        setFullscreenData(false);
      } else if (videoContainer.requestFullscreen) videoContainer.requestFullscreen();
        else if (videoContainer.mozRequestFullScreen) videoContainer.mozRequestFullScreen();
        else if (videoContainer.webkitRequestFullScreen) videoContainer.webkitRequestFullScreen();
        else if (videoContainer.msRequestFullscreen) videoContainer.msRequestFullscreen();
        setFullscreenData(true);
      }
    }
  </script>
</body>
</html>