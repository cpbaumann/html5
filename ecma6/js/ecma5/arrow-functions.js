'use strict';

// (param1, param2, …, paramN) => { statements }

var object = document.getElementsByClassName("demo");
//object.addEventListener('click', function(e){console.log(this)}); 
// object.addEventListener('click', (e) => {console.log(object)}); 


var el = document.getElementsByClassName('demo');
for (var i = 0; i < el.length; i++) {

  el[i].addEventListener('click', function (e) {
    console.log(e.target.getAttribute("data-container-value"));
    e.target.setAttribute("data-container-state", "active");
  });
}