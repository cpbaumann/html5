"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ageSymbol = Symbol();

var AnimalES6 = function () {
    function AnimalES6(name) {
        _classCallCheck(this, AnimalES6);

        this.name = name;
        this[ageSymbol] = 0;
    }

    _createClass(AnimalES6, [{
        key: "doSomething",
        value: function doSomething() {
            console.log("I'm a " + this.name);
            document.write("<p>I'm a " + this.name + "</p>");
        }
    }, {
        key: "age",
        get: function get() {
            return this[ageSymbol];
        },
        set: function set(value) {
            if (value < 0) {
                console.log("We do not support undead animals");
                document.write("<p>We do not support undead animals</p>");
            }

            this[ageSymbol] = value;
        }
    }]);

    return AnimalES6;
}();

var lionES6 = new AnimalES6("Lion");
lionES6.doSomething();
lionES6.age = 5;

var legsCountSymbol = Symbol();

var InsectES6 = function (_AnimalES) {
    _inherits(InsectES6, _AnimalES);

    function InsectES6(name) {
        _classCallCheck(this, InsectES6);

        var _this = _possibleConstructorReturn(this, (InsectES6.__proto__ || Object.getPrototypeOf(InsectES6)).call(this, name));

        _this[legsCountSymbol] = 0;
        return _this;
    }

    _createClass(InsectES6, [{
        key: "doSomething",
        value: function doSomething() {
            _get(InsectES6.prototype.__proto__ || Object.getPrototypeOf(InsectES6.prototype), "doSomething", this).call(this);
            console.log("And I have " + this[legsCountSymbol] + " legs!");
            document.write("<p>And I have " + this[legsCountSymbol] + " legs!</p>");
        }
    }, {
        key: "legsCount",
        get: function get() {
            return this[legsCountSymbol];
        },
        set: function set(value) {
            if (value < 0) {
                console.log("We do not support nether or interstellar insects");
                document.write("<p>We do not support nether or interstellar insects</p>");
            }

            this[legsCountSymbol] = value;
        }
    }]);

    return InsectES6;
}(AnimalES6);

var spiderES6 = new InsectES6("Spider");
spiderES6.legsCount = 8;
spiderES6.doSomething();