var ageSymbol = Symbol();
 
class AnimalES6 {
    constructor(name) {
        this.name = name;
        this[ageSymbol] = 0;
    }
 
    get age() {
        return this[ageSymbol];
    }
 
    set age(value) {
        if (value < 0) {
            console.log("We do not support undead animals");
            document.write("<p>We do not support undead animals</p>");
        }
 
        this[ageSymbol] = value;
    }
 
    doSomething() {
        console.log("I'm a " + this.name);
        document.write("<p>I'm a " + this.name + "</p>");
    }
}
 
var lionES6 = new AnimalES6("Lion");
lionES6.doSomething();
lionES6.age = 5;


var legsCountSymbol = Symbol();
class InsectES6 extends AnimalES6 {
    constructor(name) {
        super(name);
        this[legsCountSymbol] = 0;
    }
 
    get legsCount() {
        return this[legsCountSymbol];
    }
 
    set legsCount(value) {
        if (value < 0) {
            console.log("We do not support nether or interstellar insects");
            document.write("<p>We do not support nether or interstellar insects</p>");
        }
 
        this[legsCountSymbol] = value;
    }
 
    doSomething() {
        super.doSomething();
        console.log("And I have " + this[legsCountSymbol] + " legs!");
         document.write("<p>And I have " + this[legsCountSymbol] + " legs!</p>");
    }
}
 
var spiderES6 = new InsectES6("Spider");
spiderES6.legsCount = 8;
spiderES6.doSomething();