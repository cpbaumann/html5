
'use strict';

// (param1, param2, …, paramN) => { statements }

// object =  document.getElementsByClassName("demo");
//object.addEventListener('click', function(e){console.log(this)}); 
// object.addEventListener('click', (e) => {console.log(object)}); 

/*
 var el = document.getElementsByClassName('demo');
 for(var i=0; i<el.length; i++) {

    el[i].addEventListener('click', e => { 
    	console.log( e.target.getAttribute("data-container-value") ) 
    	e.target.setAttribute("data-container-state","active");
    }); 
 }
*/

class Test {
  constructor(classname) {
    this.classname = classname;
    this.el = document.getElementsByClassName(this.classname);
  }
  

  get doevent() {
    for(var i=0; i<this.el.length; i++) {
      this.el[i].addEventListener('click', e => { 
        console.log( e.target.getAttribute("data-container-value"));
        console.log( e.target.innerHTML);
      }); 
    }
  }
}

const te = new Test('demo');
te.doevent;


 class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
  
  get area() {
    return this.calcArea();
  }

  calcArea() {
    return this.height * this.width;
  }
}

const square = new Rectangle(10, 10);

console.log(square.area);